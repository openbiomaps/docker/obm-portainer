#!/bin/bash
# create a strong password

password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32})

echo $password > ./portainer-password

chmod 400 ./portainer-password

echo -e "\nCopy and keep safe the following password to acces the portainer app:\n"
echo $password
echo -e "\n"
